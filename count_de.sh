#!/bin/bash
#usage: sh count_de.sh filename

TEXT=$1
if [ -z "$TEXT" ]
then
	echo "Please specify a file."
	exit
fi

cat $TEXT | sed 's/\([^a-zA-Z0-9 ]\)/ & /g' $* | tr -s '\n\t ' '\n' | grep -iw 'de' | wc -w